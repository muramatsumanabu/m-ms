★最新版

部署＆支社のデータベース登録
INSERT INTO branches(
  id,
  name,
  created_date,
  updated_date
) VALUES (
  1,
  '本社',
    current_timestamp,
    current_timestamp
), (
  2,
  '支社A',
    current_timestamp,
    current_timestamp
), (
  3,
  '支社B',
    current_timestamp,
    current_timestamp
), (
  4,
  '支社C',
    current_timestamp,
    current_timestamp
);

INSERT INTO departments(
  id,
  name,
  created_date,
  updated_date
) VALUES (
  1,
  '総務人事部',
    current_timestamp,
    current_timestamp
), (
  2,
  '情報管理部',
    current_timestamp,
    current_timestamp
), (
  3,
  '営業部',
    current_timestamp,
    current_timestamp
), (
  4,
  '技術部',
    current_timestamp,
    current_timestamp
);



create table users (

id serial not null,
account varchar(20) not null,
password varchar(255) not null,
name varchar(10) not null,
branch_id integer not null,
department_id integer not null,
is_stopped smallint not null,
created_date timestamp(6) without time zone not null,
updated_date timestamp(6) without time zone not null,
primary key (id),
unique(account)

);

create table messages (

id serial not null,
title varchar(30) not null,
text text not null,
category varchar(10) not null,
user_id integer not null,
created_date timestamp(6) without time zone not null,
updated_date timestamp(6) without time zone not null,
primary key (id)

);

create table comments (

id serial not null,
text text not null,
user_id integer not null,
message_id integer not null,
created_date timestamp(6) without time zone not null,
updated_date timestamp(6) without time zone not null,
primary key (id)

);

create table branches (

id serial not null,
name varchar(255) not null,
created_date timestamp(6) without time zone not null,
updated_date timestamp(6) without time zone not null,
primary key (id)


);

create table departments (

id serial not null,
name varchar(255) not null,
created_date timestamp(6) without time zone not null,
updated_date timestamp(6) without time zone not null,
primary key (id)


);