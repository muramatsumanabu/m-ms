package com.example.demo.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

	List<Message> findByUpdatedDateBetweenAndCategoryContaining(LocalDateTime startDate, LocalDateTime endDate,
			String category);

	List<Message> findByUpdatedDateBetween(LocalDateTime startDate, LocalDateTime endDate);

}
