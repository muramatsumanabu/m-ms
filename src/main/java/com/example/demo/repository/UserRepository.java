package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	@Query(value="SELECT * FROM users WHERE account = :account AND password = :password", nativeQuery =true)
	public List<User> findAll(@Param("account") String account, @Param("password") String password);

	public Optional<User> findByAccount(String account);



}

