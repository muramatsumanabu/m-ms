package com.example.demo.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;

@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepository;

	public List<Message> findAll(String startDate, String endDate, String category) {

		if (!StringUtils.isBlank(startDate)) {
			startDate += "T00:00:00.000";

		} else {
			startDate = "2021-01-01T00:00:00.000";

		}

		if (!StringUtils.isBlank(endDate)) {
			endDate += "T23:59:59.999";

		} else {
			endDate = String.valueOf(LocalDateTime.now());
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
		LocalDateTime fmtStart = LocalDateTime.parse(startDate, formatter);
		LocalDateTime fmtEnd = LocalDateTime.parse(endDate, formatter);

		if (StringUtils.isAllBlank(category)) {
			return messageRepository.findByUpdatedDateBetween(fmtStart, fmtEnd);

		} else {
			return messageRepository.findByUpdatedDateBetweenAndCategoryContaining(fmtStart, fmtEnd, category);
		}
	}

	public void deleteById(int id) {
		messageRepository.deleteById(id);
	}

	public void saveMessage(Message message) {
		messageRepository.save(message);
	}
}
