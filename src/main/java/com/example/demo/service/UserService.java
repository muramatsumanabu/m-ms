package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public User findUser(String account, String password) {
		List<User> users = userRepository.findAll(account, password);

		if (users.isEmpty()) {
			return null;
		}  else {
			return users.get(0);
		}


	}
	public Optional<User> findById(int id) {
		return userRepository.findById(id);
	}
	public List<User> findAll(){
		return userRepository.findAll();
	}

	public void deleteById(int id) {
		userRepository.deleteById(id);
	}

	public void save(User user) {
		userRepository.save(user);
	}
	//更新
	public void saveUser(User user) {
		userRepository.save(user);
	}
	public Optional<User> findByAccount(String account){
		return userRepository.findByAccount(account);
	}

}
