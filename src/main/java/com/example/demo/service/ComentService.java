package com.example.demo.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class ComentService {
	@Autowired
	CommentRepository commentRepository;

	public List<Comment> findAll(String startDate, String endDate) {

		if (!StringUtils.isBlank(startDate)) {
			startDate += "T00:00:00.000";

		} else {
			startDate = "2021-01-01T00:00:00.000";

		}

		if (!StringUtils.isBlank(endDate)) {
			endDate += "T23:59:59.999";

		} else {
			endDate = String.valueOf(LocalDateTime.now());
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

		return commentRepository.findByUpdatedDateBetween(LocalDateTime.parse(startDate, formatter),
				LocalDateTime.parse(endDate, formatter));
	}


	public void save(Comment comment) {
		commentRepository.save(comment);
	}


	public void deleteById(int deleteId) {
		commentRepository.deleteById(deleteId);
	}


}
