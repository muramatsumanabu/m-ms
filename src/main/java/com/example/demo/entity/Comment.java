package com.example.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "comments")
public class Comment {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	//コメント
	@Column(name = "text")
	private String text;
	//ユーザーID
	@Column(name = "user_id")
	private int userId;
	//投稿ID
	@Column(name = "message_id")
	private int messageId;

	//作成日時
	@Column(name = "created_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private LocalDateTime createdDate;
	//更新日時
	@Column(name = "updated_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private LocalDateTime updatedDate;

	@Override
	public String toString() {
		return "comment [id=" + id + ", text=" + text + ", userId=" + userId + ", messageId=" + messageId
				+ ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + "]";
	}



}
