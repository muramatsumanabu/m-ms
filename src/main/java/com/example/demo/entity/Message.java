package com.example.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "messages")
public class Message {

	@Id
	@Column //カラムは複数指定可能
	//ID
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	//タイトル
	@Column(name = "title")
	private String title;
	//本文
	@Column(name = "text")
	private String text;
	//カテゴリ
	@Column(name = "category")
	private String category;
	//ユーザーID
	@Column(name = "user_id")
	private int userId;
	//作成日時
	@Column(name = "created_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private LocalDateTime createdDate;
	//更新日時
	@Column(name = "updated_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private LocalDateTime updatedDate;


	@Override
	public String toString() {
		return "Message [id=" + id + ", title=" + title + ", text=" + text + ", category=" + category + ", userId="
				+ userId + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + "]";
	}


}
