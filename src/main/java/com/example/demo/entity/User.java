package com.example.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "users")
public class User {

	@Id
	@Column //カラムは複数指定可能
	//ID
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	//アカウント
	@Column(name="account")
	private String account;
	//パスワード
	@Column(name="password")
	private String password;
	//名前
	@Column(name="name")
	private String name;
	//支店ID
	@Column(name="branch_id")
	private int branchId;
	//部署ID
	@Column(name="department_id")
	private int departmentId;
	//ユーザー停止状態
	@Column(name="is_stopped")
	private byte isStopped;
	//作成日時
	@Column(name="created_date")
	@DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS")
	private LocalDateTime createdDate;
	//更新日時
	@Column(name="updated_date")
	@DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS")
	private LocalDateTime updatedDate;

	@Override
	public String toString() {
		return "User [id=" + id + ", account=" + account + ", password=" + password + ", name=" + name + ", branchId="
				+ branchId + ", departmentId=" + departmentId + ", isStopped=" + isStopped + ", createdDate="
				+ createdDate + ", updatedDate=" + updatedDate + "]";
	}


}
