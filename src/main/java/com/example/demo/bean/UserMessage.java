package com.example.demo.bean;

import java.time.LocalDateTime;

import com.example.demo.entity.Message;
import com.example.demo.entity.User;

public class UserMessage {

	private int id;
	private String account;
	private String title;
	private String category;
	private String text;
	private int userId;
	private LocalDateTime createdDate;
	private LocalDateTime updatedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public LocalDateTime getcreatedDate() {
		return createdDate;
	}

	public void setcreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getupdatedDate() {
		return updatedDate;
	}

	public void setupdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public static UserMessage toUserMessage(User user, Message message) {
		UserMessage userMessage = new UserMessage();

		userMessage.setId(message.getId());
		userMessage.setAccount(user.getName());
		userMessage.setTitle(message.getTitle());
		userMessage.setCategory(message.getCategory());
		userMessage.setText(message.getText());
		userMessage.setUserId(message.getUserId());
		userMessage.setcreatedDate(message.getCreatedDate());
		userMessage.setupdatedDate(message.getUpdatedDate());

		return userMessage;
	}

}
