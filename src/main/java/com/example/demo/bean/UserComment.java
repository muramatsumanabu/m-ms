package com.example.demo.bean;

import java.time.LocalDateTime;

import com.example.demo.entity.Comment;
import com.example.demo.entity.User;

public class UserComment {
	private int id;
	private String account;
	private String text;
	private int userId;
	private int messageId;
	private LocalDateTime createdDate;
	private LocalDateTime updatedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public LocalDateTime getcreatedDate() {
		return createdDate;
	}

	public void setcreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getupdatedDate() {
		return updatedDate;
	}

	public void setupdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public static UserComment toUserComment(User user, Comment Comment) {
		UserComment userComment = new UserComment();

		userComment.setId(Comment.getId());
		userComment.setAccount(user.getName());
		userComment.setText(Comment.getText());
		userComment.setUserId(Comment.getUserId());
		userComment.setMessageId(Comment.getMessageId());
		userComment.setcreatedDate(Comment.getCreatedDate());
		userComment.setupdatedDate(Comment.getUpdatedDate());

		return userComment;
	}

}
