package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@Controller
public class LoginController {

	@Autowired
	HttpSession session;

	@Autowired
	UserService userService;

	@GetMapping("/loginDisplay")
	public ModelAndView loginTop() {
		ModelAndView mav = new ModelAndView();
		@SuppressWarnings("unchecked")
		List<String> errorMessages = (List<String>) session.getAttribute("errorMessages");
		mav.addObject("errorMessages", errorMessages);
		mav.setViewName("/login");
		return mav;
	}

	@PostMapping("/login")
	public ModelAndView login(@RequestParam(name = "account", required = false) String account,
			@RequestParam(name = "password", required = false) String password) {

		List<String> errorMessages = new ArrayList<>();
		User user = null;
		session.invalidate();

		if (isValid(account, password, errorMessages)) {
			user = userService.findUser(account, password);

			if (user == null || user.getIsStopped() == 0) {
				errorMessages.add("ログインに失敗しました");

			}
		}
		if (errorMessages.size() != 0) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("account", account);
			mav.setViewName("/login");
			return mav;

		} else {
			user = userService.findUser(account, password);
			session.setAttribute("loginUser", user);
			return new ModelAndView("redirect:/");

		}

	}

	public boolean isValid(String account, String password, List<String> errorMessages) {

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		}

		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");

		}
		if (errorMessages.size() == 0) {
			return true;

		}

		return false;
	}
}
