package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.MessageService;

@Controller
public class MessageController {

	@Autowired
	MessageService messageService;
	@Autowired
	HttpSession session;

	// 新規投稿画面
		@GetMapping("/newMessage")
		public ModelAndView newMessage() {
			ModelAndView mav = new ModelAndView();
			User loginUser = (User) session.getAttribute("loginUser");
			// 空のentityを準備
			Message message = new Message();
			// 画面遷移先を指定
			mav.setViewName("/newMessage");
			// 空のentityを保管
			mav.addObject("messageModel", message);
			mav.addObject("loginUser", loginUser);
			return mav;
		}

		// 投稿処理
		@PostMapping("/addMessage")
		public ModelAndView addContent(@ModelAttribute("messageModel") Message message, @RequestParam("loginUserId")int loginUserId) {
			// 新規投稿をテーブルに格納

			User loginUser = (User) session.getAttribute("loginUser");

			message.setUserId(loginUserId);
			message.setCreatedDate(LocalDateTime.now());
			message.setUpdatedDate(LocalDateTime.now());
			List<String> errorMessages = new ArrayList<>();
			ModelAndView mav = new ModelAndView();

			if(!isValid(message, errorMessages)) {

				mav.addObject("loginUser", loginUser);
				mav.addObject("errorMessages",errorMessages);
				mav.addObject("title", message.getTitle());
				mav.addObject("category", message.getCategory());
				mav.addObject("text", message.getText());
				mav.setViewName("/newMessage");
				return mav;
			}
			messageService.saveMessage(message);
			// rootへリダイレクト
			return new ModelAndView("redirect:/");
		}
		public boolean isValid(Message message, List<String> errorMessages) {

			String title = message.getTitle();
			String category = message.getCategory();
			String text = message.getText();

			if (StringUtils.isBlank(title)) {
				errorMessages.add("件名を入力してください");

			} else if (title.length() > 30) {
				errorMessages.add("件名は30字以内で入力してください");

			}

			if (StringUtils.isBlank(category)) {
				errorMessages.add("カテゴリを入力してください");

			} else if (category.length() > 10) {
				errorMessages.add("カテゴリは10字以内で入力してください");

			}

			if (StringUtils.isBlank(text)) {
				errorMessages.add("本文を入力してください");

			} else if (text.length() > 1000) {
				errorMessages.add("本文は1000字以内で入力してください");

			}

			if (errorMessages.size() == 0) {
				return true;
			}

			return false;
		}
}
