package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.ComentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.UserService;

@Controller
public class MainController {
	@Autowired
	HttpSession session;
	@Autowired
	UserService userService;
	@Autowired
	MessageService messageService;
	@Autowired
	ComentService commentService;

	@GetMapping
	public ModelAndView top(@RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate,
			@RequestParam(name = "category", required = false) String category) {

		session.removeAttribute("errorMessages");


		User loginUser = (User) session.getAttribute("loginUser");
		ModelAndView mav = new ModelAndView();
		List<User> users = userService.findAll();
		@SuppressWarnings("unchecked")
		List<String> errorMessages = (List<String>) session.getAttribute("commentErrorMessages");
		List<Message> messages = messageService.findAll(startDate, endDate, category);
		List<Comment> comments = commentService.findAll(startDate, endDate);
		messages.sort(Comparator.comparing(Message::getUpdatedDate, Comparator.reverseOrder()));
		comments.sort(Comparator.comparing(Comment::getUpdatedDate));
		Comment newComment = new Comment();

		mav.addObject("startDate", startDate);
		mav.addObject("endDate", endDate);
		mav.addObject("category", category);
		mav.addObject("hello", hello());
		mav.addObject("messages", messages);
		mav.addObject("users", users);
		mav.addObject("comments", comments);
		mav.addObject("newComment", newComment);
		mav.addObject("loginUser", loginUser);
		mav.addObject("errorMessages", errorMessages);

		session.removeAttribute("commentErrorMessages");

		mav.setViewName("/index");

		return mav;
	}

	public static String hello() {
		LocalDateTime now = LocalDateTime.now();
		int hour = now.getHour();
		System.out.println(hour);
		String hello = null;
		if (hour >= 1)
			hello = "こんばんは";
		if (hour >= 4)
			hello = "おはようございます";
		if (hour >= 10)
			hello = "こんにちは";
		if (hour >= 18)
			hello = "こんばんは";

		return hello;

	}

}
