package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.service.ComentService;

@Controller
public class DeleteCommentController {

	@Autowired
	ComentService commentService;

	//削除処理
	@PostMapping("/deleteComment/{id}")
	public ModelAndView deleteContent(@PathVariable int id) {
		commentService.deleteById(id);
		System.out.println(id);
		return new ModelAndView("redirect:/");

	}
}
