package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.service.BranchService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@Controller
public class SignupController {

	@Autowired
	UserService userService;
	@Autowired
	BranchService branchService;
	@Autowired
	DepartmentService departmentService;

	@GetMapping("/signup")
	public ModelAndView newMessage() {
		ModelAndView mav = new ModelAndView();
		User user = new User();
		List<Branch> branches = branchService.findAllBranch();
		List<Department> departments = departmentService.findAllDepartment();
		mav.setViewName("/signup");
		mav.addObject("userModel", user);
		mav.addObject("branches", branches);
		mav.addObject("departments", departments);
		return mav;
	}

	@PostMapping("/signupUser")
	public ModelAndView addContent(
			@ModelAttribute("userModel") User user,
			@RequestParam(name = "chkPassword") String chkPassword,
			@RequestParam(name = "isStopped") String isStopped,
			@RequestParam(name = "branch") int branch,
			@RequestParam(name = "department") int department) {

		ModelAndView mav = new ModelAndView();


		int isStoppedInt = Integer.parseInt(isStopped);

		List<String> errorMessages = new ArrayList<>();

		user.setIsStopped((byte) isStoppedInt);
		user.setBranchId(branch);
		user.setDepartmentId(department);
		user.setCreatedDate(LocalDateTime.now());
		user.setUpdatedDate(LocalDateTime.now());

		if (!isValid(user, chkPassword, errorMessages)) {
			List<Branch> branches = branchService.findAllBranch();
			List<Department> departments = departmentService.findAllDepartment();
			user.setPassword(null);
			mav.addObject("branches", branches);
			mav.addObject("departments", departments);
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("userModel", user);
			mav.setViewName("/signup");
			return mav;

		}
		userService.save(user);
		return new ModelAndView("redirect:/management");

	}

	public boolean isValid(User user, String chkPassword, List<String> errorMessages) {

		String account = user.getAccount();
		String password = user.getPassword();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");

		} else if (!account.matches("^[a-zA-Z0-9_\\-.]{6,20}$")) {
			errorMessages.add("アカウント名は半角英数字は6字以上20文字以下で入力してください");

		} else {
			User optUser = userService.findByAccount(account).orElse(null);

			if (optUser != null) {
				errorMessages.add("既に使用されているアカウント名です");

			}

		}
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力して下さい");

		} else if (!password.matches("^[a-zA-Z0-9.?/-]{6,20}$")) {
			errorMessages.add("パスワードは記号を含む半角文字で6文字以上20文字以下で入力してください");

		} else if (!chkPassword.equals(password)) {
			errorMessages.add("確認用パスワードに入力された値がパスワードと異なります");

		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");

		} else if (!name.matches("^.{1,10}$")) {
			errorMessages.add("名前は10文字以下で入力してください");

		}

		if (!((branchId == 1 && departmentId <= 2) || (branchId >= 2 && departmentId >= 3))) {
			errorMessages.add("支店名及び部署名の選択が不正です");

		}

		if (errorMessages.size() == 0) {
			return true;

		}

		return false;
	}

}
