package com.example.demo.controller;

import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.service.BranchService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@Controller
public class ManagementController {

	@Autowired
	HttpSession session;
	@Autowired
	UserService userService;
	@Autowired
	BranchService branchService;
	@Autowired
	DepartmentService departmentService;

	@GetMapping("/management")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();

		@SuppressWarnings("unchecked")
		List<String> errorMessages = (List<String>) session.getAttribute("signupErrorMessages");
		List<User> users = userService.findAll();
		List<Branch> branches = branchService.findAllBranch();
		List<Department> departments = departmentService.findAllDepartment();
		users.sort(Comparator.comparing(User::getId, Comparator.reverseOrder()));
		User loginUser = (User) session.getAttribute("loginUser");

		mav.setViewName("/management");
		mav.addObject("errorMessages", errorMessages);
		mav.addObject("users", users);
		mav.addObject("branches", branches);
		mav.addObject("departments", departments);
		mav.addObject("loginUser", loginUser);

		session.removeAttribute("signupErrorMessages");

		return mav;
	}

}
