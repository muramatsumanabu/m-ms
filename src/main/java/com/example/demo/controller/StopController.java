package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@Controller
public class StopController {

	@Autowired
	UserService userService;

	@PostMapping("/stop/{id}")
	public ModelAndView stop(
			@PathVariable int id,
			@RequestParam(name="userStatus") Byte isStopped) {

		Optional <User> user = userService.findById(id);
		User optedUser = user.get();
		optedUser.setIsStopped(isStopped);
		optedUser.setUpdatedDate(LocalDateTime.now());
		userService.saveUser(optedUser);
		// rootへリダイレクト
		return new ModelAndView("redirect:/management");


	}
}
