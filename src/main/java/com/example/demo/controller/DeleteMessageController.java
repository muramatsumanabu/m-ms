package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.service.MessageService;

@Controller
public class DeleteMessageController {

	@Autowired
	MessageService messageService;

	//削除処理
	@PostMapping("/deleteMessage/{id}")
	public ModelAndView deleteContent(@PathVariable int id) {
		messageService.deleteById(id);
		return new ModelAndView("redirect:/");

	}

}
