package com.example.demo.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.service.BranchService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;

@Controller
public class SettingController {

	@Autowired
	HttpSession session;
	@Autowired
	UserService userService;
	@Autowired
	BranchService branchService;
	@Autowired
	DepartmentService departmentService;

	@GetMapping("/setting/{id}")
	public ModelAndView setting(@PathVariable int id) {
		ModelAndView mav = new ModelAndView();
		List<String> illegalIdMessages = new ArrayList<>();

		if (!isValid(String.valueOf(id), illegalIdMessages)) {
			session.setAttribute("errorMessages", illegalIdMessages);
			return new ModelAndView("redirect:/management");

		}

		@SuppressWarnings("unchecked")
		List<String> errorMessages = (List<String>) session.getAttribute("errorMessages");
		List<Branch> branches = branchService.findAllBranch();
		List<Department> departments = departmentService.findAllDepartment();

		User user = (User) session.getAttribute("user");
		if(user == null) {
			user = userService.findById(id).get();
		}
		LocalDateTime createdDate = user.getCreatedDate();
		mav.addObject("errorMessages", errorMessages);
		mav.addObject("optedUser", user);
		mav.addObject("branches", branches);
		mav.addObject("departments", departments);
		mav.addObject("createdDate", createdDate);
		mav.setViewName("/setting");
		session.removeAttribute("errorMessages");
		session.removeAttribute("user");
		return mav;
	}

	@PostMapping("/updateUser/{id}")
	public ModelAndView updateContent(@PathVariable int id,
			@ModelAttribute("optedUser") User user,
			@RequestParam(name = "chkPassword") String chkPassword,
			@RequestParam(name = "branch") int branchId,
			@RequestParam(name = "department") int departmentId,
			@RequestParam(name = "createdDate") String createdDate) {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
		LocalDateTime castCreatedDate = LocalDateTime.parse(createdDate, dtf);
		user.setId(id);
		user.setBranchId(branchId);
		user.setDepartmentId(departmentId);
		user.setCreatedDate(castCreatedDate);
		user.setUpdatedDate(LocalDateTime.now());
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(user, chkPassword, errorMessages)) {
			session.setAttribute("user", user);
			session.setAttribute("errorMessages", errorMessages);
			return new ModelAndView("redirect:/setting/" + user.getId());

		}
		userService.saveUser(user);
		return new ModelAndView("redirect:/management");
	}

	public boolean isValid(User user, String chkPassword, List<String> errorMessages) {

		String account = user.getAccount();
		String password = user.getPassword();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");

		} else if (!account.matches("^[a-zA-Z0-9_\\-.]{6,20}$")) {
			errorMessages.add("アカウント名は半角英数字は6字以上20文字以下で入力してください");

		} else {
			User otherUser = userService.findByAccount(account).orElse(null);

			if (otherUser != null && otherUser.getId() != user.getId()) {
				errorMessages.add("アカウント名が重複しています");

			}

		}
		if (!StringUtils.isEmpty(password)) {

			if (!password.matches("^[a-zA-Z0-9.?/-]{6,20}$")) {
				errorMessages.add("パスワードは記号を含む半角文字で6文字以上20文字以下で入力してください");

			} else if (!chkPassword.equals(password)) {
				errorMessages.add("確認用パスワードに入力された値がパスワードと異なります");

			}
		}
		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");

		} else if (!name.matches("^.{1,10}$")) {
			errorMessages.add("名前は10文字以下で入力してください");

		}

		if (!((branchId == 1 && departmentId <= 2) || (branchId >= 2 && departmentId >= 3))) {
			errorMessages.add("支店名及び部署名の選択が不正です");

		}

		if (errorMessages.size() == 0) {
			return true;

		}

		return false;
	}

	public boolean isValid(String key, List<String> errorMessages) {

		if (StringUtils.isBlank(key)) {
			errorMessages.add("不正なパラメータです");
			return false;

		} else if (!key.matches("^[0-9]*$")) {
			errorMessages.add("不正なパラメータです");
			return false;

		}
		if (userService.findById(Integer.parseInt(key)) == null) {
			errorMessages.add("不正なパラメータです");
			return false;

		}

		if (errorMessages.size() == 0) {
			return true;

		}

		return false;
	}

}
