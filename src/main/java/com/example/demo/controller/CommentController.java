package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.ComentService;

@Controller
public class CommentController {
	@Autowired
	HttpSession session;

	@Autowired
	ComentService commentService;

	@PostMapping("newComment")
	public ModelAndView newComment(@ModelAttribute("newComment") Comment comment,
			@RequestParam("commentUserId") int userId, @RequestParam("commentMessageId") int messageId) {

		comment.setUserId(userId);
		comment.setMessageId(messageId);
		comment.setCreatedDate(LocalDateTime.now());
		comment.setUpdatedDate(LocalDateTime.now());
		List<String> errorMessages = new ArrayList<>();
		ModelAndView mav = new ModelAndView();
		if(!isValid(comment, errorMessages)) {
			mav.addObject("errorMessages", errorMessages);
			session.setAttribute("commentErrorMessages", errorMessages);

			return new ModelAndView("redirect:/") ;

		}
		commentService.save(comment);
		return new ModelAndView("redirect:/");
	}
	public boolean isValid(Comment comment, List<String> errorMessages) {

		String text = comment.getText();

		if (StringUtils.isBlank(text)) {
			errorMessages.add("コメントを入力してください");


		} else if (text.length() > 500) {
			errorMessages.add("コメントの投稿は500字以内で入力してください");

		}

		if (errorMessages.size() == 0) {
			return true;

		}

		return false;
	}
}
